package org.wit.assign01.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.wit.assign01.R;
import org.wit.assign01.app.Assign02App;

import static org.wit.android.helpers.IntentHelper.navigateUp;

/**
 * Created by My Laptop on 20/12/2015.
 */
public class LoginActivity extends Activity
{
  private Assign02App app;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    getActionBar().setDisplayHomeAsUpEnabled(true);

    app = (Assign02App) getApplication();

  }


  public void signinPressed (View view)
  {
    // gives ability to view OWN Tweets only
    TextView email     = (TextView)  findViewById(R.id.loginEmail);
    TextView password  = (TextView)  findViewById(R.id.loginPassword);

    if (app.validUser(email.getText().toString(), password.getText().toString()))
    {
      Toast toast = Toast.makeText(this, "Welcome " + app.loggedOnTweeter.firstName, Toast.LENGTH_SHORT);
      toast.show();
      app.tweetDispFollowing = false;
      startActivity (new Intent(this, TimelineActivity.class));
    }
    else
    {
      Toast toast = Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_SHORT);
      toast.show();
    }
  }


  public void followPressed (View view)
  {
    // like signinPressed but setting an app field so TimelineFragment can use this
    // to display OWN Tweets and Tweets of those you follow
    TextView email     = (TextView)  findViewById(R.id.loginEmail);
    TextView password  = (TextView)  findViewById(R.id.loginPassword);

    if (app.validUser(email.getText().toString(), password.getText().toString()))
    {
      Toast toast = Toast.makeText(this, "Welcome " + app.loggedOnTweeter.firstName, Toast.LENGTH_SHORT);
      toast.show();
      app.tweetDispFollowing = true;
      startActivity (new Intent(this, TimelineActivity.class));
    }
    else
    {
      Toast toast = Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_SHORT);
      toast.show();
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      case android.R.id.home:  navigateUp(this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }
}
