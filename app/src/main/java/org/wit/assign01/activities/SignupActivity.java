package org.wit.assign01.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import static org.wit.android.helpers.IntentHelper.navigateUp;

import org.wit.assign01.R;
import org.wit.assign01.app.Assign02App;
import org.wit.assign01.models.Tweeter;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;




/**
 * Created by My Laptop on 20/12/2015.
 */
public class SignupActivity extends Activity implements Callback<Tweeter>
{
  private Assign02App app;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_signup);
    getActionBar().setDisplayHomeAsUpEnabled(true);
    app = (Assign02App) getApplication();


  }

  public void registerPressed (View view)
  {
    TextView firstName = (TextView)  findViewById(R.id.firstName);
    TextView lastName  = (TextView)  findViewById(R.id.lastName);
    TextView email     = (TextView)  findViewById(R.id.Email);
    TextView password  = (TextView)  findViewById(R.id.Password);


    Tweeter tweeter = new Tweeter(firstName.getText().toString(), lastName.getText().toString(), email.getText().toString(), password.getText().toString());


    Call<Tweeter> call = (Call<Tweeter>) app.tweetService.createTweeter(tweeter);
    call.enqueue(this);
  }

  @Override
  public void onResponse(Response<Tweeter> response, Retrofit retrofit)
  {
    app.tweetersList.add(response.body());
    app.tweetServiceAvailable = true;
    Toast toast = Toast.makeText(this, "New Tweeter added: " + response.body().firstName, Toast.LENGTH_LONG);
    toast.show();
    startActivity(new Intent(this, WelcomeActivity.class));
  }

  @Override
  public void onFailure(Throwable t)
  {
    app.tweetServiceAvailable = false;
    Toast toast = Toast.makeText(this, "myTweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
    toast.show();
    startActivity (new Intent(this, WelcomeActivity.class));
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      case android.R.id.home:  navigateUp(this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }
}
