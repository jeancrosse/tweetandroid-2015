package org.wit.assign01.activities;

/**
 * Created by My Laptop on 24/10/2015.
 */

import java.util.ArrayList;
import java.util.List;

import org.wit.android.helpers.IntentHelper;
import org.wit.assign01.R;
import org.wit.assign01.app.Assign02App;
import org.wit.assign01.models.Collection;
import org.wit.assign01.models.Tweet;
import org.wit.assign01.models.Tweeter;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.view.ActionMode;
import android.widget.AbsListView;
import android.widget.ListView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import org.wit.assign01.settings.SettingsActivity;

public class TimelineFragment extends ListFragment implements OnItemClickListener, AbsListView.MultiChoiceModeListener, Callback<List<Tweet>>
{
  private ArrayList<Tweet> tweets;
  private Collection collection;
  private TweetAdapter adapter;
  private ListView listView;
  private Assign02App app;
  private Tweeter tweeter;
  public static final String BROADCAST_ACTION = "org.wit.assign01.activities.TimelineFragment";
  private IntentFilter intentFilter;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    app = (Assign02App) getActivity().getApplication();
    collection = app.collection;
    tweets = collection.tweets;
    tweeter   = app.loggedOnTweeter;

    adapter = new TweetAdapter(getActivity(), tweets);
    setListAdapter(adapter);

    /**
     * This refreshes Tweets from Service. Need to initially call it here to have
     * some Tweets displayed as could have none. Then, it's only called when a User
     * hits 'Refresh'. Also, if didn't have this here, the last Tweets displayed could
     * have been by a certain logged in Tweeter.
     */
    refreshTweetList();

    // Timer for pulling Tweets automatically
    intentFilter = new IntentFilter(BROADCAST_ACTION);
    registerBroadcastReceiver(intentFilter);

  }

  /**
   * Retrofit used to refresh tweet list
   */
  private void refreshTweetList()
  {
    if (app.loggedOnTweeter == null)
    {
      /* Public Timeline being viewed. Custom version of
         Call<List<Tweet>> call = app.tweetService.getAllTweets();
         as date in descending order i.e. most recent Tweet first and fetching just x
         tweets - 7 at the moment
       */

      Call<List<Tweet>> call = app.tweetService.getXTweets();
      call.enqueue(this);
    }
    else
    {
      if (app.tweetDispFollowing)
        /* Tweets pertaining to a particular user + Tweets of who they're following. Custom version of
         Call<List<Tweet>> call = app.tweetService.getTweets(app.loggedOnTweeter.id);
         as date in descending order i.e. most recent Tweet first and fetching just x
         tweets - 10 at the moment
       */
      {
        Call<List<Tweet>> call = app.tweetService.getFollowing(app.loggedOnTweeter.id);
        call.enqueue(this);
      }
      else
      {
      /* Just tweets pertaining to a particular user. Custom version of
         Call<List<Tweet>> call = app.tweetService.getTweets(app.loggedOnTweeter.id);
         as date in descending order i.e. most recent Tweet first and fetching just x
         tweets - 5 at the moment
       */

        Call<List<Tweet>> call = app.tweetService.getXOwnTweets(app.loggedOnTweeter.id);
        call.enqueue(this);
      }
    }
  }

  @Override
  public void onResponse(Response<List<Tweet>> response, Retrofit retrofit)
  {

    List<Tweet> list = response.body();


    //delete all local tweets first then add tweets from service
    collection.removeTweets();
    collection.updateTweet(list);
    ((TweetAdapter) getListAdapter()).notifyDataSetChanged();
    Toast.makeText(getActivity(), "Retrieved " + list.size() + " tweets", Toast.LENGTH_LONG).show();
  }

  @Override
  public void onFailure(Throwable t)
  {
    Toast.makeText(getActivity(), "Failed to retrieve tweet list", Toast.LENGTH_LONG).show();
  }


  private void registerBroadcastReceiver(IntentFilter intentFilter)
  {
    ResponseReceiver responseReceiver = new ResponseReceiver();
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(responseReceiver, intentFilter);
  }

  //Broadcast receiver for receiving status updates from the IntentService
  private class ResponseReceiver extends BroadcastReceiver
  {
    // Called when the BroadcastReceiver gets an Intent it's registered to receive
    @Override
    public void onReceive(Context context, Intent intent)
    {

      //delete all local tweets first then add tweets from service
      Toast.makeText(getActivity(), "Got Timer Tweets", Toast.LENGTH_LONG).show();
      collection.removeTweets();
      collection.updateTweet(app.tweetList);
      ((TweetAdapter) getListAdapter()).notifyDataSetChanged();

    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
  {
    View v = super.onCreateView(inflater, parent, savedInstanceState);
    listView = (ListView) v.findViewById(android.R.id.list);
    listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
    listView.setMultiChoiceModeListener(this);

    return v;
  }

  /* ************ MultiChoiceModeListener methods (begin) *********** */
  @Override
  public boolean onCreateActionMode(ActionMode actionMode, Menu menu)
  {
    MenuInflater inflater = actionMode.getMenuInflater();
    inflater.inflate(R.menu.tweet_list_context, menu);
    return true;
  }

  @Override
  public boolean onPrepareActionMode(ActionMode actionMode, Menu menu)
  {
    return false;
  }

  @Override
  public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem)
  {
    switch (menuItem.getItemId())
    {
      case R.id.menu_item_delete_tweet:
        if (app.loggedOnTweeter == null)
        {
          // put out toast as deleting individual Tweet needs Tweeter id for route
          Toast.makeText(getActivity(), "Need to be logged on to delete a Tweet", Toast.LENGTH_LONG).show();
          actionMode.finish();
          adapter.notifyDataSetChanged();
        }
        else
        {
          deleteTweet(actionMode);
        }
        return true;

      default:
        return false;
    }

  }

  private void deleteTweet(ActionMode actionMode)
  {
    for (int i = adapter.getCount() - 1; i >= 0; i--)
    {
      if (listView.isItemChecked(i))
      {
        // delete locally
        //collection.deleteTweets(adapter.getItem(i));

        //delete on Service then need to hit refresh
        deleteOneRemoteTweet(adapter.getItem(i));
      }
    }
    actionMode.finish();
    adapter.notifyDataSetChanged();
  }

  private void deleteOneRemoteTweet(Tweet delTweet)
  {
    Call<String> call = app.tweetService.deleteTweet(app.loggedOnTweeter.id, delTweet.id);
    call.enqueue(new Callback<String>()
    {

      @Override
      public void onResponse(Response<String> response, Retrofit retrofit)
      {
        Toast.makeText(getActivity(), "Tweet deleted in Service: " + response.body(), Toast.LENGTH_SHORT).show();
      }

      @Override
      public void onFailure(Throwable t)
      {
        Toast.makeText(getActivity(), "Failed to delete the tweet", Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  public void onDestroyActionMode(ActionMode actionMode)
  {
  }

  @Override
  public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked)
  {
  }

  /* ************ MultiChoiceModeListener methods (end) *********** */


  @Override
  public void onListItemClick(ListView l, View v, int position, long id)
  {
    Tweet twit = ((TweetAdapter) getListAdapter()).getItem(position);
    Intent i = new Intent(getActivity(), TweetPagerActivity.class);
    i.putExtra(TweetFragment.EXTRA_TWEET_ID, twit.id);
    startActivityForResult(i,0);
  }

  @Override
  public void onResume()
  {
    super.onResume();
    ((TweetAdapter) getListAdapter()).notifyDataSetChanged();
  }

  public void onPause()
  {
    /*this is to prevent a user clicking clear on the list, getting
    out of the app, killing it, then reclicking on it and the
    old list is still there as we haven't saved to json*/

    super.onPause();
    collection.saveTweets();
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
  {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.menu_timeline, menu);
  }

  public void clearTweets()
  {
    //remove Tweets locally from Collection
    collection.removeTweets();

    //remove Tweets locally from Service
    deleteAllRemoteTweets();

    // refresh the activity after tweets cleared otherwise they'll be removed but still display
    getActivity().finish();
    startActivity(getActivity().getIntent());
    Toast toast = Toast.makeText(getActivity(), "Tweets Cleared", Toast.LENGTH_SHORT);
    toast.show();
  }

  private void deleteAllRemoteTweets()
  {
    Call<String> call = app.tweetService.deleteAllTweets();
    call.enqueue(new Callback<String>() {

      @Override
      public void onResponse(Response<String> response, Retrofit retrofit)
      {
        Toast.makeText(getActivity(), "All Tweets deleted: "+response.body(), Toast.LENGTH_SHORT).show();
      }

      @Override
      public void onFailure(Throwable t)
      {
        Toast.makeText(getActivity(), "Failed to delete all tweets", Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      case R.id.menu_item_new_tweet:
        if (app.loggedOnTweeter == null)
        {
          Toast.makeText(getActivity(), "Need to logon to create a new Tweet", Toast.LENGTH_LONG).show();
        }
        else
        {
          Intent i = new Intent(getActivity(), TweetPagerActivity.class);
          startActivityForResult(i, 0);
        }
        return true;

      case R.id.action_refresh:
        refreshTweetList();
        return true;

      case R.id.action_clear : clearTweets();
        return true;

      case R.id.action_settings:
        Intent j = new Intent(getActivity(), SettingsActivity.class);
        startActivityForResult(j, 0);
        return true;

      // replaced by Timer settings activity
      //case R.id.action_settings : startActivity (new Intent(getActivity(), SettingsActivityold.class));
      //case R.id.action_settings:

        //Intent j = new Intent(getActivity(), SettingsActivityold.class);
        //startActivityForResult(j, 0);
        //return true;

      // just for mobile use as my mobile doesn't display overflow menu
      case R.id.action_settings2:
        Intent n = new Intent(getActivity(), SettingsActivity.class);
        startActivityForResult(n, 0);
        return true;


      // A User can get to Timeline by not logging in and seeing all Tweets or logging
      // in and just seeing their Tweets. Didn't want to implement Up navigation as needed
      // to log a Tweeter out if logged in. The semantics of calling the menu item 'Logout'
      // seemed incorrect if no one was logged in but the same way has to be used to exit
      // from this screen in both cases so called it 'Exit'
      case R.id.menuExit:
        if (app.loggedOnTweeter != null)
        {
          Toast.makeText(getActivity(), "Goodbye " + app.loggedOnTweeter.firstName, Toast.LENGTH_LONG).show();
          app.loggedOnTweeter = null;
        }
        else
        {
          Toast.makeText(getActivity(), "Goodbye ", Toast.LENGTH_LONG).show();
        }
        Intent k = new Intent(getActivity(), WelcomeActivity.class);
        startActivityForResult(k, 0);
        return true;

      // does the same as overflow menu item exit but was needed for logging a Tweeter
      // out on my phone which doesn't show the overflow menu
      case R.id.action_exit:
        if (app.loggedOnTweeter != null)
        {
          Toast.makeText(getActivity(), "Goodbye " + app.loggedOnTweeter.firstName, Toast.LENGTH_LONG).show();
          app.loggedOnTweeter = null;
        }
        else
        {
          Toast.makeText(getActivity(), "Goodbye ", Toast.LENGTH_LONG).show();
        }
        Intent m = new Intent(getActivity(), WelcomeActivity.class);
        startActivityForResult(m, 0);
        return true;


      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id)
  {
    Tweet tweet = adapter.getItem(position);
    IntentHelper.startActivityWithData(getActivity(), TweetPagerActivity.class, "TWEET_ID", tweet.id);
  }

  class TweetAdapter extends ArrayAdapter<Tweet>
  {
    private Context context;
    private int maxDisplayTweetText = 28;
    private String displayTweetText;

    public TweetAdapter(Context context, ArrayList<Tweet> tweets)
    {
      super(context, 0, tweets);
      this.context = context;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
      LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      if (convertView == null)
      {
        convertView = inflater.inflate(R.layout.list_item_tweet, null);
      }
      Tweet squawk = getItem(position);

      displayTweetText = squawk.tweetText.length() > maxDisplayTweetText ? squawk.tweetText.substring(0, maxDisplayTweetText) + "..." : squawk.tweetText;

      TextView one_tweetText = (TextView) convertView.findViewById(R.id.list_item_tweetText);
      one_tweetText.setText(displayTweetText);


      TextView one_tweetDate = (TextView) convertView.findViewById(R.id.list_item_tweetDate);
      one_tweetDate.setText(squawk.getDateString());

      return convertView;
    }
  }
}
