package org.wit.assign01.activities;

/**
 * Created by My Laptop on 24/10/2015.
 */
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;
import static org.wit.android.helpers.ContactHelper.getEmail;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import android.app.Activity;
import android.content.Intent;
import org.wit.assign01.app.Assign02App;
import org.wit.assign01.models.Collection;
import org.wit.assign01.models.Tweet;
import org.wit.assign01.R;
import org.wit.assign01.models.Tweeter;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.wit.android.helpers.MapHelper;
import android.support.v4.app.FragmentManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;



import static org.wit.android.helpers.IntentHelper.sendEmail;
import static org.wit.android.helpers.IntentHelper.navigateUp;

public class TweetFragment extends Fragment implements TextWatcher,
                                                       OnClickListener,
                                                       Callback<Tweet>,
                                                       GoogleMap.OnMarkerDragListener,
                                                       GoogleMap.OnCameraChangeListener

{
  public static   final String  EXTRA_TWEET_ID = "assign01.TWEET_ID";
  private static final int REQUEST_CONTACT = 1;

  private Button emailTweetButton;
  private Button selContactButton;
  private Button tweetSubmitButton;
  private TextView tweetDate;
  private TextView tweetCount;
  private TextView tweetText;

  private Tweet tweet;
  private Tweet tweetCreate;
  private Date  date;
  private Date  dateToSave;
  private Assign02App app;
  private Collection collection;
  private int maxTweetLength = 140;
  private boolean TweetPressedBefore = false;
  private String emailContact = "";
  private Tweeter tweeter;

  // Map fields
  SupportMapFragment mapFragment;
  GoogleMap gmap;
  Marker marker;
  LatLng markerPosition;
  String currentGeolocation;// = "52.253456,-7.187162"
  public HashMap<Marker, String> markers = new HashMap<>();


  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);


    app = (Assign02App) getActivity().getApplication();
    collection = app.collection;
    tweeter   = app.loggedOnTweeter;

  /* Needed a way to check if the call from Timeline was to
   create a new Tweet or pick one from a list in which case there
   would be arguments of the tweetId. I do not create a tweetID when
   a User selects a new tweet from Timeline as they may not follow
   through. I only create it when they actually submit the tweet.
   Need to check if there are arguments and only appoint a UUID if so*/


    Bundle arguments = getArguments();

    if (arguments == null)
    {
      Log.v("Tweet", "TweetFragment arguments are null");
      tweet = null;
    }
    else
    {
      if(arguments.containsKey(EXTRA_TWEET_ID))
      {
        //UUID tweetId = (UUID)getArguments().getSerializable(EXTRA_TWEET_ID);
        String tweetId = (String)getArguments().getSerializable(EXTRA_TWEET_ID);
        tweet = collection.getTweet(tweetId);
        Log.v("Tweet", "TweetFragment arguments are NOT null");
      }
      else
      {
        tweet = null;
        Log.v("Tweet", "TweetFragment arguments are NOT null but don't contain EXTRA_TWEET_ID");
      }
    }


  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
  {
    super.onCreateView(inflater, parent, savedInstanceState);
    View v = inflater.inflate(R.layout.fragment_tweet, parent, false);

    getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
    addRWidgets(v);

    if (tweet !=null)
    //means User has selected a tweet from TimeLine list view
    {
      updateControls(tweet);
    }
    else
    //means User has opted to enter a new Tweet
    {
      setWidgetInitialVal();
    }

    return v;
  }

  private void setWidgetInitialVal()
  {
    //initial settings for widgets - only called on new Tweet
    tweetCount.setText(Integer.toString(maxTweetLength));


    date = new Date();
    DateFormat df = DateFormat.getDateTimeInstance();
    df.setTimeZone(TimeZone.getTimeZone("GMT"));
    tweetDate.setText(df.format(date));
    dateToSave = new Date(df.format(date));



    //set listeners for tweet submit button and text changing
    tweetSubmitButton .setOnClickListener(this);
    tweetText         .addTextChangedListener(this);
  }

  private void addRWidgets(View v)
  {
    //called when a new tweet or a list selection
    emailTweetButton  = (Button)  v.findViewById(R.id.emailTweetButton);
    tweetSubmitButton = (Button)  v.findViewById(R.id.tweetSubmitButton);
    selContactButton  = (Button)  v.findViewById(R.id.selContactButton);
    tweetCount        = (TextView)  v.findViewById(R.id.tweetCount);
    tweetDate         = (TextView)  v.findViewById(R.id.tweetDate);
    tweetText         = (TextView) v.findViewById(R.id.tweetText);

    tweetCount.setTextColor(Color.BLACK);

    /*these below are the only listeners set up both when a new tweet
    can be entered and also when select a previous tweet from a list.
    Hence, they are separate from the other listeners in setWidgetInitialVal(),
    which only apply to a new tweet. These listeners don't exist for a previous
    tweet. This is the only method called by both ways to reach this activity.*/
    selContactButton.setOnClickListener(this);
    emailTweetButton.setOnClickListener(this);
  }

  public void updateControls(Tweet tweet)
  {
    //if a User has chosen an individual tweet from the Timeline, display it
    tweetText.setText(tweet.tweetText);
    tweetCount.setText(tweet.tweetCount);
    tweetDate.setText(tweet.getDateString());

    //disable Tweet button when displaying a previous tweet - no listener set either
    tweetSubmitButton.setEnabled(false);

    //make tweetText read only while displaying a previous tweet - no listener set either
    tweetText.setEnabled(false);

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      case android.R.id.home: navigateUp(getActivity());
        return true;
      default:                return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void onPause()
  {
    super.onPause();
    collection.saveTweets();
  }


  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after)
  { }

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count)
  {}

  @Override
  public void afterTextChanged(Editable c)
  {
    /*
    Decrement the tweetCount counter from its start of 140 by 1 for
    each letter typed in the multiline tweetText
    if the tweetCount counter goes to 0, change its colour to red + send toast
    if has become red and you delete a character, colour stays red so
    need an else to go back to black
    */

    tweetCount.setText(Integer.toString(maxTweetLength - tweetText.length()));
    if (maxTweetLength - tweetText.length() == 0)
    {
      Toast toast = Toast.makeText(getActivity(), "Maximum number of Characters reached for Tweet!", Toast.LENGTH_SHORT);
      toast.show();
      tweetCount.setTextColor(Color.RED);
    }
    else
    {
      tweetCount.setTextColor(Color.BLACK);
    }


  }

  // Implement the Callback methods
  @Override
  public void onResponse(Response<Tweet> response, Retrofit retrofit)
  {
    Tweet returnedTweet = response.body();
    //Log.v("Tweet", "In onResponse returnedTweet " + returnedTweet.tweetText);

    //Log.v("Tweet", "In onResponse tweetCreate " + tweetCreate.tweetText);

    // If returned tweet matches the tweet we created and transmitted to the server then success.

    if (tweetCreate.equals(returnedTweet))
    {
      Toast.makeText(getActivity(), "Tweet created successfully in service", Toast.LENGTH_LONG).show();
    }
    else
    {
      Toast.makeText(getActivity(), "Failed to create tweet in service", Toast.LENGTH_LONG).show();
    }
  }

  @Override
  public void onFailure(Throwable t)
  {
    Toast.makeText(getActivity(), "Failed to create tweet due to unknown network issue", Toast.LENGTH_LONG).show();
  }


  private void tweetSubmit()
  {
    /*Only want to save Tweets to Tweet model class if User actually enters something
    Pressing Tweet button generates 'Message Sent' toast. Also ensure they don't
    inadvertently press the Tweet button twice on the same tweet*/



    if (!tweetText.getText().toString().equals("") && !TweetPressedBefore)
    {
      Toast toast = Toast.makeText(getActivity(), "Message Sent", Toast.LENGTH_SHORT);
      toast.show();

      /* Tweeter geolocation is in format (latitude,longitude) but latitude and longitude are separate fields in
         Tweet model. Find "," position and pull out the separate fields
       */

      int commaPos = app.loggedOnTweeter.geolocation.indexOf(",");
      String latitude = app.loggedOnTweeter.geolocation.substring(0, commaPos);
      String longitude = app.loggedOnTweeter.geolocation.substring((commaPos + 1),app.loggedOnTweeter.geolocation.length());

      tweetCreate = new Tweet(tweetText.getText().toString(), tweetCount.getText().toString(), dateToSave, latitude, longitude);

      collection.addTweet(tweetCreate);

      transmitTweet(tweetCreate);

      TweetPressedBefore = true;

      //disable tweetText so user can't change and tweet a different message with the same timestamp
      tweetText.setEnabled(false);

    }
    else
    {
      if (TweetPressedBefore)
      {
        Toast toast = Toast.makeText(getActivity(), "You've already tweeted this text!!!!", Toast.LENGTH_SHORT);
        toast.show();
      }
      else
      // no text entered
      {
        Toast toast = Toast.makeText(getActivity(), "No Tweet entered - please try again or exit", Toast.LENGTH_SHORT);
        toast.show();
      }
    }

  }

   public void transmitTweet(Tweet tweetCreate)
   {
     // Make the API call once the tweet has been fully formed.
     Call<Tweet> call = app.tweetService.createTweet(app.loggedOnTweeter.id, tweetCreate);
     call.enqueue(this);
   }

  private void tweetEmail()
  {

    // Two ways to get Tweet text
    String tweetMessage = "";


    // a tweet object is available if tweet selected from Timeline

    if (tweet != null)
    {
      tweetMessage = tweet.tweetText;
    }

    // This value is set if User chooses to add a Tweet and then clicks the 'Tweet' button
    if (TweetPressedBefore)
    {
      tweetMessage = tweetText.getText().toString();
    }

    /*check if the tweet has been tweeted. If not, force user to do it before
    selecting email. You should only be able to email a Tweet i.e. text you have tweeted
    Then, check that user has selected a Contact to mail*/


    if (!tweetMessage.equals(""))
    {
      if (emailContact.equals(""))
      // i.e. email address has never been retrieved and user has clicked 'email'
      {
        Toast toast = Toast.makeText(getActivity(), "Select a Contact before mailing", Toast.LENGTH_SHORT);
        toast.show();
      }
      else
      {
        sendEmail(getActivity(), emailContact, getString(R.string.tweet_report_subject), tweetMessage);
      }
    }
    else
    {
      Toast toast = Toast.makeText(getActivity(), "Can only email a Tweet - need to Tweet!", Toast.LENGTH_SHORT);
      toast.show();
    }
  }





  @Override
  public void onClick(View v)
  {
    switch (v.getId())
    {
      case R.id.tweetSubmitButton      : tweetSubmit();
        break;
      case R.id.selContactButton       : Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(i, REQUEST_CONTACT);
        break;
      case R.id.emailTweetButton       : tweetEmail();
        break;

    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {

    /*in myrent, the Tenant contact was saved as it made sense in the portfolio of
    residences that we kept details on the tenant renting. Have decided not to save the
    email of who we tweet to as we could tweet to many or the next time we view the tweet,
    we might email to someone else. Didn't make sense to save it as it wasn't necessary for
    the Tweet model.*/

    if (resultCode != Activity.RESULT_OK)
    {
      Log.v("Tweet", "TweetFragment - onActivityResult from get contacts is: " + resultCode);
      return;
    }
    else
    {
      switch (requestCode)
      {
        case REQUEST_CONTACT:
          emailContact = getEmail(getActivity(), data);
          selContactButton.setText(emailContact);
          break;
      }
    }
  }

  //-------------------------Google Map------------------------------------------//
    /*
   * This Camera refers to Google map camera, not device camera.
   * When camera changes, this equivalent to changed map position here.
   * For example by panning map to new position:
   */
  @Override
  public void onCameraChange(CameraPosition cameraPosition)
  {
    app.loggedOnTweeter.zoom = cameraPosition.zoom;
    addVisibleRegionMarkers();
  }

  @Override
  public void onMarkerDragStart(Marker marker) {}

  @Override
  public void onMarkerDrag(Marker marker) {}

  @Override
  public void onMarkerDragEnd(Marker marker)
  {
    currentGeolocation = MapHelper.latLng(marker.getPosition());
    getActivity().setTitle(currentGeolocation);
    app.loggedOnTweeter.geolocation = currentGeolocation;
    gmap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
  }

  private void initializeMapFragment()
  {
    FragmentManager fm = getChildFragmentManager();
    mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
    if (mapFragment == null)
    {
      mapFragment = SupportMapFragment.newInstance();
      fm.beginTransaction().replace(R.id.map, mapFragment).commit();
    }
  }

  /**
   * Make markers visible that are present within visible map region
   */
  public void addVisibleRegionMarkers()
  {
    // Obtain the bounds of the visible map
    LatLngBounds bounds = gmap.getProjection().getVisibleRegion().latLngBounds;

    Set keys = markers.keySet();
    Iterator it = keys.iterator();
    while(it.hasNext())
    {
      Marker marker = (Marker)it.next();
      if(bounds.contains(marker.getPosition()))
      {
        marker.setVisible(true);
      }
      else
      {
        marker.setVisible(false);
      }
    }
  }

  /*
  * initializes and renders the map
  * sets map type, example HYBRID in this case
  * sets the drag marker listener (but does not create a marker)
  * sets the map (camera) changed listener
  */
  private void renderMap(LatLng markerPosition)
  {
    if (mapFragment != null)
    {
      gmap = mapFragment.getMap();
      if (gmap != null)
      {
        gmap.animateCamera(CameraUpdateFactory.newLatLngZoom(markerPosition, (float)app.loggedOnTweeter.zoom));
        gmap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        gmap.setOnMarkerDragListener(this);
        gmap.setOnCameraChangeListener(this);//this Camera refers to Google map, not device camera
        createMarkersWeakHashMap();
      }
    }
  }

  /*
  * Create hash map of markers representing all tweeter locations.
  * None of markers is draggable except that of logged in tweeter.
  * Logged in tweeter marker coloured blue.
  * Remaining markers default red.
  * Set all markers invisible here.
  */
  public void createMarkersWeakHashMap()
  {
    List<Tweeter> tweeters = app.tweetersList;
    Iterator<Tweeter> it = tweeters.iterator();
    boolean isDraggable = false;

    while(it.hasNext())
    {
      Tweeter tweeter = it.next();
      boolean isLoggedInTweeter = tweeter.id.equals(app.loggedOnTweeter.id);
      // only logged in tweeter marker draggable
      isDraggable = isLoggedInTweeter == true ? true : false;
      LatLng geolocation = MapHelper.latLng(getActivity(), tweeter.geolocation);

      MarkerOptions options = new MarkerOptions()
          .position(geolocation)
          .draggable(isDraggable)
          .visible(false)
          .alpha(0.7f)
          .title("Tweeter Location")
          .snippet("GPS : " + tweeter.geolocation);
      Marker marker = gmap.addMarker(options);

      // Logged in tweeter marker coloured differently
      if (isLoggedInTweeter)
        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

      markers.put(marker, tweeter.id);
    }
  }

  /* @see http://stackoverflow.com/questions/18206615/how-to-use-google-map-v2-inside-fragment
   * Necessary to wait until MapFragment created before initializing map fragment
   * Note that map fragment is nested within residence fragment. See the xml layout file for residence frag
  */
  @Override
  public void onActivityCreated(Bundle savedInstanceState)
  {
    super.onActivityCreated(savedInstanceState);
    currentGeolocation = app.loggedOnTweeter.geolocation;
    initializeMapFragment();
  }

  @Override
  public void onStart()
  {
    super.onStart();
    renderMap(MapHelper.latLng(getActivity(), currentGeolocation));
  }
  //-------------------------End Google Map--------------------------------------//

}
