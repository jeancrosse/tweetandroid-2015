package org.wit.assign01.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import org.wit.assign01.R;
import org.wit.assign01.app.Assign02App;
import org.wit.assign01.models.Tweeter;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by My Laptop on 20/12/2015.
 */
public class WelcomeActivity extends Activity  implements Callback<List<Tweeter>>
{
  private Assign02App app;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_welcome);

    app = (Assign02App) getApplication();

  }

  @Override
  public void onResume()
  {
    super.onResume();
    app.loggedOnTweeter = null;
    app.tweetDispFollowing = false;
    Call<List<Tweeter>> call = (Call<List<Tweeter>>) app.tweetService.getAllTweeters();
    call.enqueue(this);
  }

  @Override
  public void onResponse(Response<List<Tweeter>> response, Retrofit retrofit)
  {

    app.tweetersList = response.body();
    app.tweetServiceAvailable = true;
    int numberTweeters = app.tweetersList.size();
    Toast.makeText(this, "Retrieved " + numberTweeters + " tweeters", Toast.LENGTH_LONG).show();

  }

  @Override
  public void onFailure(Throwable t)
  {
    app.tweetServiceAvailable = false;
    serviceUnavailableMessage();
  }


  public void loginPressed (View view)
  {
    if (app.tweetServiceAvailable)
    {
      startActivity (new Intent(this, LoginActivity.class));
    }
    else
    {
      serviceUnavailableMessage();
    }
  }


  public void signupPressed (View view)
  {
    if (app.tweetServiceAvailable)
    {
      startActivity (new Intent(this, SignupActivity.class));
    }
    else
    {
      serviceUnavailableMessage();
    }
  }

  public void allTweetsPressed (View view)
  {
    startActivity (new Intent(this, TimelineActivity.class));
  }

  void serviceUnavailableMessage()
  {
    Toast toast = Toast.makeText(this, "myTweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
    toast.show();
  }

  void serviceAvailableMessage()
  {
    Toast toast = Toast.makeText(this, "myTweet Service Contacted Successfully", Toast.LENGTH_LONG);
    toast.show();
  }



}
