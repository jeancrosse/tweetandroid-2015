package org.wit.assign01.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import static org.wit.android.helpers.IntentHelper.navigateUp;
import org.wit.assign01.R;
import android.text.TextWatcher;


/**
 * Created by My Laptop on 26/10/2015.
 */
public class SettingsActivityold extends Activity implements TextWatcher
{
  private TextView settingsRefresh;
  private TextView settingsPassword;
  private TextView settingsName;

  private String saveName     = "";
  private String savePassword = "";
  private String saveRefresh  = "";

  public static final String MyPREFERENCES = "MyPrefs" ;
  public static final String Name = "nameKey";
  public static final String Password = "passwordKey";
  public static final String Refresh = "refreshKey";
  SharedPreferences sharedpreferences;



  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_settings);
    getActionBar().setDisplayHomeAsUpEnabled(true);

    settingsName        = (TextView)     findViewById(R.id.settingsName);
    settingsPassword    = (TextView)     findViewById(R.id.settingsPassword);
    settingsRefresh     = (TextView)     findViewById(R.id.settingsRefresh);

    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

    if (sharedpreferences == null)
    {
      Log.v("Tweet", "shared prefs are null");
    }
    else
    {
      settingsName.setText(sharedpreferences.getString(Name,""));
      settingsPassword.setText(sharedpreferences.getString(Password, ""));
      settingsRefresh.setText(sharedpreferences.getString(Refresh,""));
    }

    //add listeners
    settingsName.addTextChangedListener(this);
    settingsPassword.addTextChangedListener(this);
    settingsRefresh.addTextChangedListener(this);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      case android.R.id.home:  navigateUp(this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void afterTextChanged(Editable s)
  {

    //compare hashcode of fields to the editable hashcode to id which one was changed
    String text = s.toString();

    if (settingsName.getText().hashCode() == s.hashCode())
    {
      saveName = text;
    }

    if (settingsPassword.getText().hashCode() == s.hashCode())
    {
      savePassword = text;
    }

    if (settingsRefresh.getText().hashCode() == s.hashCode())
    {
      saveRefresh = text;
    }

    /*Had originally planned only to save the Setting to Shared Preferences if all
    3 input fields were changed but realised that a User could just change one and expect
    them to be saved. Can change any or all of the preferences */




    if (!saveName.equals("") || !savePassword.equals("") || !saveRefresh.equals(""))
    {
      // create Shared Preferences
      SharedPreferences.Editor editor = sharedpreferences.edit();

      if (!saveName.equals(""))
        editor.putString(Name, saveName);

      if (!savePassword.equals(""))
       editor.putString(Password, savePassword);

      if (!saveRefresh.equals(""))
        editor.putString(Refresh, saveRefresh);

       editor.commit();
    }


  }

  @Override
  public void beforeTextChanged(CharSequence c, int start, int count, int after)
  {
  }

  @Override
  public void onTextChanged(CharSequence c, int start, int count, int after)
  {
  }
}
