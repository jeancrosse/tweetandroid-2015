package org.wit.assign01.activities;

/**
 * Created by My Laptop on 25/10/2015.
 */

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import org.wit.assign01.R;
import org.wit.assign01.app.Assign02App;
import org.wit.assign01.models.Collection;
import org.wit.assign01.models.Tweet;
import java.util.ArrayList;
import java.util.UUID;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import static org.wit.android.helpers.LogHelpers.info;


public class TweetPagerActivity extends FragmentActivity implements ViewPager.OnPageChangeListener
{
  private ViewPager viewPager;
  private ArrayList<Tweet> tweets;
  private Collection collection;
  private PagerAdapter pagerAdapter;


  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);

   /* In myrent, an extra of residence id is always setup from the residencelist
    if you click + to create a new residence i.e. residence is instantiated there.
    I only create a tweet in TweetFragment if a user clicks 'Tweet' button, hence had to
    put in extra code to check if no extras existed i.e. user clicked on new tweet as opposed
    to user clicking on a tweet in the Timeline list and having extras and setting up paging*/

    Bundle extras = getIntent().getExtras();
    if (extras == null)
    {
      Log.v("Tweet", "TweetPagerActivity extras are null");
      setContentView(R.layout.fragment_container);

      FragmentManager manager = getSupportFragmentManager();
      Fragment fragment = manager.findFragmentById(R.id.fragmentContainer);
      if (fragment == null)
      {
        fragment = new TweetFragment();
        manager.beginTransaction().add(R.id.fragmentContainer, fragment).commit();
      }
    }
    else
    {
      Log.v("Tweet", "TweetPagerActivity extras are NOT null");
      viewPager = new ViewPager(this);
      viewPager.setId(R.id.viewPager);
      setContentView(viewPager);
      setTweetList();
      pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tweets);
      viewPager.setAdapter(pagerAdapter);
      viewPager.addOnPageChangeListener(this);
      setCurrentItem();
    }

  }

  private void setTweetList()
  {
    Assign02App app = (Assign02App) getApplication();
    collection = app.collection;
    tweets = collection.tweets;
  }

  /*
  * Ensure selected residence is shown in details view
  */
  private void setCurrentItem()
  {
    //UUID twet = (UUID) getIntent().getSerializableExtra(TweetFragment.EXTRA_TWEET_ID);
    String twet = (String) getIntent().getSerializableExtra(TweetFragment.EXTRA_TWEET_ID);
    for (int i = 0; i < tweets.size(); i++)
    {
     // if (tweets.get(i).id.toString().equals(twet.toString()))
        if (tweets.get(i).id.equals(twet))
      {
        viewPager.setCurrentItem(i);
        break;
      }
    }
  }

  @Override
  public void onPageScrolled(int arg0, float arg1, int arg2)
  {
    info(this, "onPageScrolled: arg0 " + arg0 + " arg1 " + arg1 + " arg2 " + arg2);
    Tweet tweet = tweets.get(arg0);
    if (tweet.tweetDate != null)
    //display "mytweet" string with just the tweet date (and not time) on the Action Bar
    {
      setTitle(getResources().getString(R.string.label_mytweet) + " " + tweet.getDateBarString());
    }
  }


  @Override
  public void onPageSelected(int position)
  {

  }

  @Override
  public void onPageScrollStateChanged(int state)
  {

  }

  class PagerAdapter extends FragmentStatePagerAdapter
  {
    private ArrayList<Tweet>  tweets;

    public PagerAdapter(FragmentManager fm, ArrayList<Tweet> tweets)
    {
      super(fm);
      this.tweets = tweets;
    }

    @Override
    public int getCount()
    {
      return tweets.size();
    }

    @Override
    public Fragment getItem(int pos)
    {
      Tweet tweet = tweets.get(pos);
      Bundle args = new Bundle();
      args.putSerializable(TweetFragment.EXTRA_TWEET_ID, tweet.id);
      TweetFragment fragment = new TweetFragment();
      fragment.setArguments(args);
      return fragment;
    }
  }
}
