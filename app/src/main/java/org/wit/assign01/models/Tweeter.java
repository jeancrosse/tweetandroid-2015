package org.wit.assign01.models;

/**
 * Created by My Laptop on 09/10/2015.
 */
import java.util.UUID;

public class Tweeter
{
  public String id;
  public String firstName;
  public String lastName;
  public String email;
  public String password;
  public String geolocation;
  public double zoom;


  public Tweeter(String firstName, String lastName, String email, String password)
  {
    this.id = UUID.randomUUID().toString();
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.geolocation = "52.253456,-7.187162";
    this.zoom = 16.0f;
  }

}