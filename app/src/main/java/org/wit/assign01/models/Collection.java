package org.wit.assign01.models;

/**
 * Created by My Laptop on 10/10/2015.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import android.util.Log;

import static org.wit.android.helpers.LogHelpers.info;

public class Collection
{
  public  ArrayList<Tweet>  tweets;
  private CollectionSerializer   serializer;

  public Collection(CollectionSerializer serializer)
  {
    this.serializer = serializer;
    try
    {
      tweets = serializer.loadTweets();
    }
    catch (Exception e)
    {
      info(this, "Error loading tweets: " + e.getMessage());
      tweets = new ArrayList<Tweet>();
    }



  }

  public boolean saveTweets()
  {
    try
    {
      serializer.saveTweets(tweets);
      info(this, "Tweets saved to file");
      return true;
    }
    catch (Exception e)
    {
      info(this, "Error saving tweets: " + e.getMessage());
      return false;
    }
  }


  public void addTweet(Tweet tweet)
  {
    tweets.add(tweet);
  }

  public void removeTweets()
  {
    tweets.clear();
  }

  //public Tweet getTweet(UUID id)
  public Tweet getTweet(String id)
  {
    Log.i(this.getClass().getSimpleName(), "UUID parameter id: " + id);

    for (Tweet squawk : tweets)
    {
      if(id.equals(squawk.id))
      {
        return squawk;
      }
    }
    info(this, "failed to find tweet. returning first element array to avoid crash");
    return null;
  }

  public void updateTweet(List<Tweet> list)
  {
    tweets.addAll(list);
  }

  public void deleteTweets(Tweet c)
  {
    tweets.remove(c);
  }

}
