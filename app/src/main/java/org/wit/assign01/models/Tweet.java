package org.wit.assign01.models;

/**
 * Created by My Laptop on 09/10/2015.
 */
import java.util.Objects;
import java.util.UUID;
import java.util.Date;
import java.text.DateFormat;

import org.wit.assign01.R;
import android.content.Context;

//needed for JSON
import org.json.JSONException;
import org.json.JSONObject;

public class Tweet
{
  //public UUID   id;
  public String id;
  public String tweetText;
  public String tweetCount;
  public Date tweetDate;
  public String latitude;
  public String longitude;


  private static final String JSON_ID             = "id"            ;
  private static final String JSON_TWEET          = "tweetText"     ;
  private static final String JSON_DATE           = "date"          ;
  private static final String JSON_COUNT          = "count"         ;
  private static final String JSON_LATITUDE       = "latitude"      ;
  private static final String JSON_LONGITUDE      = "longitude"     ;

  public Tweet(String tweetText, String tweetCount, Date tweetDate, String latitude, String longitude)
  //public Tweet(String tweetText, String tweetCount, String tweetDate)
  {
    this.id          = UUID.randomUUID().toString();
    this.tweetText   = tweetText;
    this.tweetCount  = tweetCount;
    this.tweetDate   = tweetDate;
    this.latitude    = latitude;
    this.longitude   = longitude;
  }

  public String getDateString()
  {
    return DateFormat.getDateTimeInstance().format(tweetDate);
  }

  // when paging through tweets, used to display date in Action Bar, without time
  public String getDateBarString()
  {
    return DateFormat.getDateInstance().format(tweetDate);
  }

  public Tweet(JSONObject json) throws JSONException
  {

    //id            = UUID.fromString(json.getString(JSON_ID));
    id            = json.getString(JSON_ID);
    tweetText     = json.getString(JSON_TWEET);
    tweetCount    = json.getString(JSON_COUNT);
    tweetDate     = new Date(json.getLong(JSON_DATE));
    latitude      = json.getString(JSON_LATITUDE);
    longitude     = json.getString(JSON_LONGITUDE);

  }

  public JSONObject toJSON() throws JSONException
  {
    //save a Tweet object to JSON
    JSONObject json = new JSONObject();


    //json.put(JSON_ID            , id.toString());
    json.put(JSON_ID            , id);
    json.put(JSON_TWEET         , tweetText);
    json.put(JSON_DATE          , tweetDate.getTime());
    json.put(JSON_COUNT         , tweetCount);
    json.put(JSON_LATITUDE      , latitude);
    json.put(JSON_LONGITUDE     , longitude);

    return json;
  }



  public boolean equals(final Tweet obj)
  {
    if (obj instanceof Tweet)
    {
      final Tweet other = (Tweet) obj;
      return id.equals(other.id)
          && tweetText.equals(other.tweetText)
          && tweetCount.equals(other.tweetCount)
          && latitude.equals(other.latitude)
          && longitude.equals(other.longitude);


      //&& tweetDate.equals(other.tweetDate);
    }
    else
    {
      return false;
    }
  }


}
