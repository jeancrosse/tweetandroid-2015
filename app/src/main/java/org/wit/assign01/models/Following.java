package org.wit.assign01.models;

/**
 * Created by My Laptop on 09/10/2015.
 */


import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;


public class Following
{
  public Tweeter sourceTweeter;
  public Tweeter targetTweeter;

  public Following(Tweeter source, Tweeter target)
  {
    sourceTweeter = source;
    targetTweeter = target;
  }

}
