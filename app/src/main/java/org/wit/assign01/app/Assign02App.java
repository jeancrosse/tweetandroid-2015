package org.wit.assign01.app;

/**
 * Created by My Laptop on 10/10/2015.
 */
import org.wit.assign01.app.services.RefreshService;
import org.wit.assign01.main.MyTweetServiceProxy;
import org.wit.assign01.models.Collection;
import org.wit.assign01.models.CollectionSerializer;

import android.app.Application;
import static org.wit.android.helpers.LogHelpers.info;

import org.wit.assign01.main.MyTweetServiceProxy;
import org.wit.assign01.models.Tweet;
import org.wit.assign01.models.Tweeter;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;


public class Assign02App extends Application
{
  private static final String FILENAME = "collection.json";
  public Collection collection;


  //public String          service_url  = "http://10.0.2.2:9000";    //Android Emulator
  //public String          service_url  = "http://10.0.3.2:9000";    //Genymotion
  //public String          service_url  = "https://donation-service-2015.herokuapp.com/";
  public String          service_url  = "https://wit-mytweet2015-jcrosse.herokuapp.com";

  public MyTweetServiceProxy tweetService;
  public boolean             tweetServiceAvailable = false;
  public boolean             tweetDispFollowing    = false;
  public List<Tweeter> tweetersList = new ArrayList<Tweeter>();
  public List <Tweet>  tweetList    = new ArrayList<Tweet>();
  public Tweeter loggedOnTweeter;
  private Timer timer;

  @Override
  public void onCreate()
  {
    super.onCreate();
    CollectionSerializer serializer = new CollectionSerializer(this, FILENAME);
    collection = new Collection(serializer);

    Gson gson = new GsonBuilder()
        .setDateFormat(DateFormat.LONG)
        .create();
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(service_url)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build();
    tweetService = retrofit.create(MyTweetServiceProxy.class);

    info(this, "MyTweet app launched");
    timer = new Timer();
    refreshTweetList();
  }

  public void newUser(Tweeter tweeter)
  {
    tweetersList.add(tweeter);
  }

  public boolean validUser (String email, String password)
  {
    for (Tweeter user : tweetersList)
    {
      if (user.email.equals(email) && user.password.equals(password))
      {
        loggedOnTweeter = user;
        return true;
      }
    }
    return false;
  }


  public void refreshTweetList()
  {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

    // Parmeter 2 is default val in minutes set at 1 minute here for test purposes.
    // The preference key *refresh_interval* is defined in *res/xml/settings.xml*.
    String refreshInterval = prefs.getString("refresh_interval", "1");
    // Precondition: user-input refresh frequency units are minutes.
    // Convert refreshFrequency minutes to milliseconds.
    int refreshFrequency = Integer.parseInt(refreshInterval) * 60 * 1000;
    int initialDelay = 1000; // Set an initial delay of 1 second

    timer.schedule(new TimerTask()
    {
      @Override
      public void run()
      {
        startService(new Intent(getBaseContext(), RefreshService.class));

      }
    }, initialDelay, refreshFrequency);
  }





}
