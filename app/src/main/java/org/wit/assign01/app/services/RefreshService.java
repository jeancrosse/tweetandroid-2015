package org.wit.assign01.app.services;

/**
 * Created by My Laptop on 28/12/2015.
 */

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import org.wit.android.helpers.LogHelpers;
import org.wit.assign01.activities.TimelineFragment;
import org.wit.assign01.app.Assign02App;
import org.wit.assign01.models.Tweet;

import java.io.IOException;
import java.util.List;

import retrofit.Call;
import retrofit.Response;

public class RefreshService extends IntentService
{
  private String tag = "Tweet";
  Assign02App app;


  public RefreshService()
  {
    super("RefreshService");

  }

  @Override

  protected void onHandleIntent(Intent intent)
  {
    /* A User can view Timeline Tweets in 3 ways - 1) All Tweets (getXTweets - 7 at the moment),
       2) When logged on can view 5 of their own Tweets (getXOwnTweets),
       3) When logged on can view 10 combined of their own tweets and those they follow.
    */


    Log.v("Tweet", "RefreshService onHandleIntent running");
    app = (Assign02App) getApplication();
    Intent localIntent = new Intent(TimelineFragment.BROADCAST_ACTION);

    Call<List<Tweet>> call = (Call<List<Tweet>>) app.tweetService.getAllTweets();
    if (app.loggedOnTweeter == null)
    {
      call = (Call<List<Tweet>>) app.tweetService.getXTweets();
    }
    else
    {
      if (app.tweetDispFollowing)
      {
        call = (Call<List<Tweet>>) app.tweetService.getFollowing(app.loggedOnTweeter.id);
      }
      else
      {
        call = (Call<List<Tweet>>) app.tweetService.getXOwnTweets(app.loggedOnTweeter.id);
      }
    }




    try
    {
      Response<List<Tweet>> response = call.execute();
      app.tweetList = response.body();
      LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }
    catch (IOException e)
    {
      LogHelpers.info(tag, "Failed to retrieve tweet list - network error");
    }

  }

  @Override
  public void onDestroy()
  {
    super.onDestroy();
    LogHelpers.info(this, "onDestroyed");
  }
}
