package org.wit.assign01.main;

/**
 * Created by My Laptop on 27/11/2015. Service Proxy needed to talk to Service
 */
import org.wit.assign01.models.Tweet;
import org.wit.assign01.models.Tweeter;


import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface MyTweetServiceProxy
{

  @GET("/api/tweeters")
  Call<List<Tweeter>> getAllTweeters();

  @GET("/api/tweeters/{id}")
  Call<Tweeter> getTweeter(@Path("id") String id);

  @POST("/api/tweeters")
  Call<Tweeter> createTweeter(@Body Tweeter tweeter);

  @DELETE("/api/tweeters/{id}")
  Call<Tweeter> deleteTweeter(@Path("id") String id);

  @DELETE("/api/tweeters")
  Call<String> deleteAllTweeters();

  @GET("/api/tweets")
  Call<List<Tweet>> getAllTweets();

  // a custom version of getAllTweets with date sorted desc and returning x tweets
  @GET("/api/tweetsx")
  Call<List<Tweet>> getXTweets();

  //custom version of getTweets to fetch a certain number of logged in user's
  // Tweets and those of Tweeters they're following in date descending order
  @GET("/api/tweeters/{id}/tweetfol")
  Call<List<Tweet>> getFollowing(@Path("id") String id);


  @DELETE("/api/tweets")
  Call<String> deleteAllTweets();

  @GET("/api/tweeters/{id}/tweets")
  Call<List<Tweet>> getTweets(@Path("id") String id);

  // a custom version of getTweets with date sorted desc and returning x tweets
  @GET("/api/tweeters/{id}/tweetsx")
  Call<List<Tweet>> getXOwnTweets(@Path("id") String id);

  @GET("/api/tweeters/{id}/tweets/{tweetId}")
  Call<Tweet> getTweet(@Path("id") String id, @Path("tweetId") String tweetId);

  @POST("/api/tweeters/{id}/tweets")
  Call<Tweet> createTweet(@Path("id") String id, @Body Tweet tweet);

  @DELETE("/api/tweeters/{id}/tweets/{tweetId}")
  Call<String> deleteTweet(@Path("id") String id, @Path("tweetId") String tweetId);



}
