package org.wit.android.helpers;

/**
 * Created by My Laptop on 17/10/2015.
 */
import android.util.Log;

public class LogHelpers
{
  public static void info(Object parent, String message)
  {
    Log.i(parent.getClass().getSimpleName(), message);
  }
}

